# tradeLifeCyclePart3

A trade, also called a deal, is an exchange of financial products from one entity to another. The life cycle of a trade is the fundamental activity of exchanges, investment banks, hedge funds, pension funds and many other financial companies.